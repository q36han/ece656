import pymysql


def main():
    db = pymysql.connect(host="localhost", user="root", password="123456", database="finalproject")
    cursor = db.cursor()

    command = input("Welcome to the US accident database, press 'c' to continue: ")
    while command == 'c':
        choice = input("Press 'c' to continue or press 'e' to exit the program: ")
        if choice == 'e':
            exit(0)
        elif choice == 'c':
            selection = input("Please choose from the following(enter number): \n"
                              "1. check basic information about a certain accident\n"
                              "2. check the weather condition of a certain accident\n"
                              "3. check the manner of collision of a certain accident\n"
                              "4. check the lighting condition of a certain accident\n"
                              "5. check total number of accidents in a certain city\n"
                              "6. check the type of intersection of the location of a certain accident\n"
                              "7, check the precise location of a certain accident")

            if selection == '5':
                cityName = input("Please enter the city name(all capital letters): ")
                sql = "SELECT COUNT(*) FROM accident inner join geolocation_unitedstates ON " \
                      "accident.CITY = geolocation_unitedstates.City_Code WHERE City_Name = '%s'" % cityName

                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        sum = row[0]
                        print("\nCity: %s,Total accident number: %s\n\n" % (
                            cityName, sum))

                except:
                    print("Error: unable to fetch data")
            elif selection == '2':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "SELECT ST_CASE,atmospheric_conditions.weather FROM accident inner join atmospheric_conditions  " \
                      "ON accident.WEATHER = atmospheric_conditions.id WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        id = row[0]
                        weather = row[1]
                        print("\nCase Number: %s,Weather: %s\n\n" % (
                            id, weather))
                except:
                    print("Error: unable to fecth data")
            elif selection == '3':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "SELECT ST_CASE ,manner_collision.manner_of_collision FROM accident inner join manner_collision " \
                      "ON accident.MAN_COLL = manner_collision.id WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        id = row[0]
                        weather = row[1]
                        print("\nCase Number: %s,Manner of Collision: %s\n\n" % (
                            id, weather))
                except:
                    print("Error: unable to fetch data")
            elif selection == '4':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "SELECT ST_CASE ,light_condition.light_condition FROM accident inner join light_condition " \
                      "ON accident.LGT_COND = light_condition.id WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        id = row[0]
                        weather = row[1]
                        print("\nCase Number: %s,Light Condition: %s\n\n" % (
                            id, weather))
                except:
                    print("Error: unable to fetch data")
            elif selection == '1':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "SELECT ST_CASE,DAY,MONTH,YEAR,PERSONS,functional_system,ownership,work_zone FROM accident inner join " \
                      "functional_system ON accident.FUNC_SYS = functional_system.id inner join ownership on " \
                      "accident.RD_OWNER = ownership.id inner join work_zone on accident.WRK_ZONE = work_zone.id " \
                      "WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        id = row[0]
                        day = row[1]
                        month = row[2]
                        year = row[3]
                        persons = row[4]
                        sys = row[5]
                        owner = row[6]
                        zone = row[7]

                        print("\nCase Number: %s, happens on:  %s/%s/%s, %s person(s) involved, surrounding work "
                              "activity: %s\nAccident route owned by: %s, The function of the road is: %s\n\n" % (
                                  id, day, month, year, persons, zone, owner, sys))
                except:
                    print("Error: unable to fetch data")
            elif selection == '6':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "select type_of_intersection from accident inner join intersection on accident.TYP_INT = " \
                      "intersection.id WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        type = row[0]

                        print("\nCase Number: %s, the type of intersection of crash location is: %s\n\n" % (
                            caseID, type))
                except:
                    print("Error: unable to fetch data")
            elif selection == '7':
                caseID = input("Please enter the case ID that you are searching for: ")
                sql = "select State_Name,City_Name,LATITUDE,LONGITUD from accident inner join " \
                      "geolocation_unitedstates on accident.STATE = geolocation_unitedstates.State_Code and " \
                      "accident.CITY = geolocation_unitedstates.City_Code WHERE ST_CASE = '%d'" % int(caseID)
                try:
                    cursor.execute(sql)
                    results = cursor.fetchall()
                    for row in results:
                        state = row[0]
                        city = row[1]
                        la = row[2]
                        lo = row[3]

                        print(
                            "\nCase Number: %s, the detailed location is: %s in %s, Latitude: %s, Longitude: %s\n\n" % (
                                caseID, state, city, la, lo))
                except:
                    print("Error: unable to fetch data")
    db.close()


main()
