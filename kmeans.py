import mysql.connector
from sklearn.mixture import GaussianMixture
from sklearn.cluster import k_means, KMeans
import numpy as np
import matplotlib.pyplot as plt

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Leonard2",
    database="ECE656"
)

mycursor = mydb.cursor()

mycursor.execute(
    "SELECT HOUR, MINUTE FROM ACCIDENT where DAY_WEEK<6")

time = [x+y/60 for x, y in mycursor]
arr = np.array(time)
arr = arr.reshape(-1, 1)
kmeans = KMeans(n_clusters=2).fit(arr)
print(kmeans.labels_)
mu_0 = 5.0
srd_0 = 2.0
hx, hy, _ = plt.hist(arr, bins=96, density=1, color="lightblue")

plt.ylim(0.0, max(hx)+0.05)
plt.title('Accident in One Day')
plt.grid()

plt.xlim(0, mu_0+10*srd_0)

plt.savefig("example_gmm_01.png", bbox_inches='tight')
plt.show()
""">>> import numpy as np
>>> from sklearn.mixture import GaussianMixture
>>> X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
>>> gm = GaussianMixture(n_components=2, random_state=0).fit(X)
>>> gm.means_
array([[10.,  2.],
       [ 1.,  2.]])
>>> gm.predict([[0, 0], [12, 3]])
array([1, 0])
"""
